package inheritance;

public class Parent {

	public void sayHello() {
		System.out.println("Hello I am a " + this.getClass());
	}
	
}
