# Inheritance

This project demonstrates basic inheritance in Java.

The "Parent" class implements a method called "sayHello".

The "Child" class extends (inherits from) the "Parent" class.

You are able to call "sayHello" on the "Child" class.

The "Child" has inherited the method from the "Parent"